package com.example.nnesterov.lruexample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.LruCache;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.net.URL;

public class CachingImageProvider {
    private static final String TAG = CachingImageProvider.class.getSimpleName();
    private static final int CACHE_SIZE = 15 * 1024 * 1024;

    private final LruCache<String, Bitmap> imageCache = new LruCache<String, Bitmap>(CACHE_SIZE){

        @Override
        protected int sizeOf(String key, Bitmap value) {
            return value.getByteCount();
        }

        @Override
        protected Bitmap create(String key) {
            InputStream is = null;
            try {
                Thread.sleep(1000);
                is = (InputStream) new URL(key).getContent();
                return BitmapFactory.decodeStream(is);
            } catch (Exception e) {
                return null;
            } finally {
                IOUtils.closeQuietly(is);
            }
        }

        @Override
        protected void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue) {
            super.entryRemoved(evicted, key, oldValue, newValue);
            Log.d(TAG, "Removed " + key);
        }
    };


    public Bitmap get(String key) {
        return imageCache.get(key);
    }
}
