package com.example.nnesterov.lruexample;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ImageAdapter extends BaseAdapter {
    private final Context context;
    private final LayoutInflater layoutInflater;
    private final List<String> urls;
    private final CachingImageProvider imageProvider = new CachingImageProvider();
    private final ExecutorService executor = Executors.newCachedThreadPool();

    private final Map<View, Future> loadTasks = new IdentityHashMap<>();

    public ImageAdapter(Context context, List<String> urls) {
        this.context = context;
        this.urls = new ArrayList<>(urls);
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return urls.size();
    }

    @Override
    public String getItem(int position) {
        return urls.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View result = convertView != null ? convertView : createView();
        Future loadTask = loadTasks.get(result);
        if (loadTask != null) {
            loadTask.cancel(false);
        }

        ViewHolder holder = (ViewHolder) result.getTag();
        holder.imageView.setImageResource(android.R.color.transparent);
        String url = getItem(position);
        Future newLoadTask = executor.submit(new LoadImageRunnable(url, holder.imageView));
        loadTasks.put(result, newLoadTask);
        return result;
    }

    public View createView() {
        View view = layoutInflater.inflate(R.layout.item, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
        ViewHolder holder = new ViewHolder(imageView);
        view.setTag(holder);
        return view;
    }


    private static class ViewHolder {
        final ImageView imageView;

        ViewHolder(ImageView imageView) {
            this.imageView = imageView;
        }
    }


    private class LoadImageRunnable implements Runnable {
        private final String imageUrl;
        private final WeakReference<ImageView> destinationImageView;

        private LoadImageRunnable(String imageUrl, ImageView destinationImageView) {
            this.imageUrl = imageUrl;
            this.destinationImageView = new WeakReference<ImageView>(destinationImageView);
        }

        @Override
        public void run() {
            final Bitmap bitmap = imageProvider.get(imageUrl);
            final ImageView destination = destinationImageView.get();
            if (destination != null) {
                destination.post(new Runnable() {
                    @Override
                    public void run() {
                        destination.setImageBitmap(bitmap);
                    }
                });
            }
        }
    }
}
