package com.example.nnesterov.lruexample;

import java.util.ArrayList;
import java.util.List;

public final class ImageUrlGenerator {
    private static final String[] imageIds = {"159029",
            "20008",
            "155060",
            "286092",
            "100075",
            "61060",
            "46076",
            "301007",
            "26031",
            "232038",
            "45077",
            "365025",
            "188091",
            "299091",
            "181079",
            "22090",
            "370036",
            "15088",
            "22093",
            "376020",
            "187071",
            "105053",
            "271008",
            "277095",
            "198023"};

    private static final String URL = "https://www.eecs.berkeley.edu/Research/Projects/CS/vision/bsds/BSDS300/html/images/plain/normal/color/%s.jpg";


    private ImageUrlGenerator() {
    }


    public static List<String> generate() {
        List<String> result = new ArrayList<>(imageIds.length);
        for (String imageId : imageIds) {
            result.add(String.format(URL, imageId));
        }
        return result;
    }
}
